import json
import socket
import syslog

def util(config, args):
    """
    Main utility function
    :param:   config ConfigParser.ConfigParser
    :param:   args Arguments
    :return:  None
    """
    handle_utils(config, args)


def handle_utils(config, args):
    handled = False
    if args.delete:
        handled = True
        handle_delete(args.delete, config)
    if args.add:
        handled = True
        handle_add(args.add, config)
    if not handled:
        print "Nothing requested... exiting."


def handle_delete(delete_key, config):
    store, kwargs = init_storage(config)
    with store.Store(**kwargs) as storage:
        storage.delete(delete_key)


def handle_add(add_key, config):
    store, kwargs = init_storage(config)
    with store.Store(**kwargs) as storage:
        storage.add(add_key)


def init_storage(config):
    type = config.get('server', 'storage')
    storage = __import__('storage.' + type, fromlist=[type])
    kwargs = {}
    if config.has_section(type):
        kwargs = {v[0]: v[1] for v in config.items(type)}
    return storage, kwargs