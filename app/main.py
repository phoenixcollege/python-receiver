import json
import socket
import syslog
from .util import init_storage

def main(config):
    """
    Main application function
    :param:   config ConfigParser.ConfigParser
    :return:  None
    """
    listen(config)


def listen(config):
    address = config.get('server', 'address')
    port = config.get('server', 'port')
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((address, int(port)))
    store, kwargs = init_storage(config)
    while True:
        data, addr = sock.recvfrom(1024)
        if data and addr:
            try:
                conv_data = json.loads(data)
                with store.Store(**kwargs) as storage:
                    storage.store(conv_data, addr)
            except ValueError as e:
                syslog.syslog(syslog.LOG_ERR, "Receiver error: {}".format(e))
                pass
