import datetime

class Store:

    def __init__(self, **kwargs):
        self.data = {}
        self.kwargs = kwargs
        self.init()

    def get_id(self, data):
        if 'host' in data:
            return data['host']

    def old_data_has_id(self, id):
        return id in self.data

    def init(self):
        pass

    def store(self, data, address):
        """
        Override in modules to create data
        :return: None
        """
        id = self.get_id(data)
        data['address'] = address
        data['timestamp'] = "{:%Y-%m-%d %H:%M:%S}".format(datetime.datetime.now())
        self._store(id, data, address)

    def delete(self, delete_key):
        print "Attempting to remove key: {}".format(delete_key)
        if self._delete(delete_key):
            print "Key deleted"
        else:
            print "Unable to delete key"

    def add(self, add_key):
        print "Attempting to add key: {}".format(add_key)
        if self._add(add_key):
            print "Key added"
        else:
            print "Unable to add key"

    def do_cleanup(self):
        pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.do_cleanup()

    def _store(self, id, data, address):
        pass

    def _delete(self, key):
        pass

    def _add(self, key):
        pass
