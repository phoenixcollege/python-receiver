from .abstract import Store as Parent
import json
import os

class Store(Parent):

    def get_path(self, file_path):
        try:
            i = file_path.index('/')
            if i == 0:
                return file_path
        except ValueError:
            pass
        return os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', file_path)

    def init(self):
        self.fp = None
        try:
            self.fp = open(self.get_path(self.kwargs['path']), 'r')
            try:
                self.data = json.load(self.fp)
            except ValueError:
                pass
        except IOError:
            pass
        self.do_cleanup()

    def _store(self, id, data, address):
        self.fp = open(self.get_path(self.kwargs['path']), 'w')
        self.data[id] = data
        self.fp.truncate()
        json.dump(self.data, self.fp)
        self.do_cleanup()

    def _delete(self, key):
        if key in self.data:
            self.fp = open(self.get_path(self.kwargs['path']), 'w')
            del self.data[key]
            self.fp.truncate()
            json.dump(self.data, self.fp)
            self.do_cleanup()
            return True
        return False

    def _add(self, key):
        if key in self.data:
            return True
        else:
            self.store({'host': key}, None)
        return True

    def do_cleanup(self):
        if self.fp:
            self.fp.close()
            self.fp = None
