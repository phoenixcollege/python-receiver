from app.util import util
import argparse
import ConfigParser
import os
if __name__ == "__main__":
    '''entry point into utilities'''
    config = ConfigParser.ConfigParser(allow_no_value=True)
    config.readfp(open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'receiver.cfg')))
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', dest='config', help="Path to config file")
    parser.add_argument('-d', '--delete', dest='delete', help="Name to remove from storage")
    parser.add_argument('-a', '--add', dest='add', help="Add a key to storage")
    args = parser.parse_args()
    if args.config:
        config.read([args.config])
    util(config, args)

